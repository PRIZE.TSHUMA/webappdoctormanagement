﻿using System.Web;
using System.Web.Optimization;

namespace WebAppDoctorManagement
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                       "~/Scripts/jquery-{version}.js",
                       "~/Scripts/jquery.unobtrusive-ajax.js",
                       "~/Scripts/jquery.inputmask.bundle.min.js"
                       ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
        "~/Scripts/jquery-ui-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/DataTables/css/dataTables.bootstrap4.min.css",
            //          "~/Content/themes/base/jquery-ui.css",
            //          "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/DataTables/jqueryDataTables").Include(
                 "~/Content/DataTables/css/dataTables.jqueryui.css"//,
                  //"~/Content/DataTables/css/dataTables.bootstrap.css"
                 )
                 );

            bundles.Add(new ScriptBundle("~/bundles/DataTables/jqueryDataTables").Include(
                      "~/Scripts/DataTables/jquery.dataTables.js"
                      //"~/Scripts/DataTables/dataTables.bootstrap.min.js",
                      //"~/Scripts/colResizable-1.6.*"
                      )
                      );

            bundles.Add(new ScriptBundle("~/bundles/DoctorManagement").Include(
           "~/Scripts/DoctorManagement.js"));
        }
    }
}
