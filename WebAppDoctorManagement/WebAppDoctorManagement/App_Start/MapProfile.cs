﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAppDoctorManagement.Models;

namespace WebAppDoctorManagement.App_Start
{
    internal class MapProfile : AutoMapper.Profile
    {
        protected void Configure()
        {
            CreateMap<Province, ProvinceViewModel>();
            CreateMap<ProvinceViewModel, Province>();
            
            CreateMap<Title, TitleViewModel>();
            CreateMap<TitleViewModel, Title>();

            CreateMap<Region, RegionViewModel>();
            CreateMap<RegionViewModel, Region>();
            
            CreateMap<DoctorProfile, DoctorProfileViewModel>();
            CreateMap<DoctorProfileViewModel, DoctorProfile>();
        }
    }
}
