﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace WebAppDoctorManagement.Aspects
{ 
    /// <summary>
  /// this Attribute class checks if the user role is valid to access the system functionality
  /// </summary>
  /// <seealso cref="System.Web.Mvc.AuthorizeAttribute" />
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    class AuthoriseUserManagementRoleAttribute : System.Web.Mvc.AuthorizeAttribute
    {
        /// <summary>
        /// Gets or sets the allowed roles.
        /// </summary>
        /// <value>
        /// The allowed roles.
        /// </value>
        public int[] AllowedRoles { get; set; }

        /// <summary>
        /// Called when a process requests authorization.
        /// </summary>
        /// <param name="filterContext">The filter context, which encapsulates information for using <see cref="T:System.Web.Mvc.AuthorizeAttribute" />.</param>
        /// <exception cref="System.UnauthorizedAccessException"></exception>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {

            if (HttpContext.Current.Session.Count == 0)
            {
                throw new UnauthorizedAccessException("Your do not have permission to access the system.");
            }

            base.OnAuthorization(filterContext);

            if (filterContext.Result != null && typeof(HttpUnauthorizedResult) == filterContext.Result.GetType())
            {
                throw new UnauthorizedAccessException("Your do not have permission to access this part of the system.");
            }

            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                base.HandleUnauthorizedRequest(filterContext);
            }

            if (HttpContext.Current.Session.Count == 0)
            {
                throw new UnauthorizedAccessException("Your do not have permission to access the system");
            }
        }
         
    }
}
