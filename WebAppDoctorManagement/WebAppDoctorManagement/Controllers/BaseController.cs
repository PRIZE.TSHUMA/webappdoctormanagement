﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppDoctorManagement.Filters;

namespace WebAppDoctorManagement.Controllers
{
    [ExceptionLoggingFilter]
    public abstract class BaseController : Controller
    {
        internal virtual ActionResult RaiseError(string error)
        {
            var errors = new List<string> {
            error
          };

            return Json(new { success = false, errors = errors.ToArray(), JsonRequestBehavior = JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);
        }

        internal virtual ActionResult RaiseError(List<string> errorList)
        {
            return Json(new { success = false, errors = errorList.ToArray() }, JsonRequestBehavior.AllowGet);
        }

        internal virtual ActionResult RaiseSuccess()
        {
            return Json(new { success = true });
        }

        internal virtual ActionResult RaiseSuccess(string informationMessage)
        {
            return Json(new { success = true, InformationMessage = informationMessage }, JsonRequestBehavior.AllowGet);
        }
    }
}