﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppDoctorManagement.DAL;
using WebAppDoctorManagement.Models;

namespace WebAppDoctorManagement.Controllers
{
    [Authorize]
    public class DoctorController : BaseController
    {
        // GET: Doctor
        public ActionResult Index()
        {
            List<DoctorProfileViewModel> model = new List<DoctorProfileViewModel>();
            using (DoctorManager doctorMnger = new DoctorManager())
            {
                var modelDoctorProfileDB = doctorMnger.GetAllDoctorProfile();
                Mapper.Configuration.CreateMapper();
                model = Mapper.Map<List<DoctorProfile>, List<DoctorProfileViewModel>>(modelDoctorProfileDB);
            }
            return PartialView(model);
        }

        public ActionResult CaptureDoctorProfile()
        {
            AccountController accController = new AccountController();

            if(isAdminUser() == false)
            {
                return PartialView("Error", new HandleErrorInfo(new Exception("Your do not have permission to access this part of the system."), "CaptureDoctorProfile", "CaptureDoctorProfile"));
            }

            Mapper.Configuration.CreateMapper();
            CaptureDoctorProfileViewModel model = new CaptureDoctorProfileViewModel();

            using (TitleManager titleMnger = new TitleManager())
            {
                var titleDB = titleMnger.GetAllTitle();
                model.TitleList = Mapper.Map<List<Title>, List<TitleViewModel>>(titleDB);
            }

            using (RegionManager regionMnger = new RegionManager())
            {
                var regionDB = regionMnger.GetAllRegion();
                model.RegionList = Mapper.Map<List<Region>, List<RegionViewModel>>(regionDB);
            }

            using (ProvinceManager provinceMnger = new ProvinceManager())
            {
                var provinceDB = provinceMnger.GetAllProvince();
                model.ProvinceList = Mapper.Map<List<Province>, List<ProvinceViewModel>>(provinceDB);
            }
            
            return View(model);
        }

        [HttpPost]
        public ActionResult AddDoctorProfile(CaptureDoctorProfileViewModel model)
        {
            bool result = false;
            if (ModelState.IsValid)
            {
                using (DoctorManager courseMngr = new DoctorManager())
                {
                    DoctorProfile updateDBModel = new DoctorProfile();
                    updateDBModel.Id = model.Id;
                    updateDBModel.FirstName = model.FirstName;
                    updateDBModel.Surname = model.Surname;
                    updateDBModel.HPCSANUMBER = model.HPCSANUMBER;
                    updateDBModel.IDNumber = model.IDNumber;
                    updateDBModel.Discipline = model.Discipline;
                    updateDBModel.TitleId = model.TitleId;
                    updateDBModel.ProvinceId = model.ProvinceId;
                    updateDBModel.RegionId = model.RegionId;
                    result = courseMngr.CreateDoctorProfile(updateDBModel);
                }
            }
            else
            {
                return base.RaiseError("Invalid do information.");
            }

            if (result)
            {
                return base.RaiseSuccess("Updated Successfully");
            }
            else
            {
                return base.RaiseError("Update Failed");
            }
        }

        public Boolean isAdminUser()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var s = UserManager.GetRoles(user.GetUserId());
                if (s != null && s.Any() && s[0].ToString() == "Admin")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
    }
}