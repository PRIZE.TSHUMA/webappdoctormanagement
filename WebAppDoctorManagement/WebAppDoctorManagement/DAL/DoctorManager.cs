﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAppDoctorManagement.Models;

namespace WebAppDoctorManagement.DAL
{
  
   public class DoctorManager : IDisposable
    {
        private readonly ApplicationDbContext _context = new ApplicationDbContext();

        public List<DoctorProfile> GetAllDoctorProfile()
        {
            return _context.DoctorProfiles.Include("Province").Include("Region").Include("Title").ToList();
        }

        public bool CreateDoctorProfile(DoctorProfile model)
        {
            bool result = false;
            _context.DoctorProfiles.Add(model);
            _context.SaveChanges();
            result = true;
            return result;
        }

        #region IDisposable Support
        private bool disposedValue = false;

        private void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing && _context != null)
                {
                    _context.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion IDisposable Support
    }
}
