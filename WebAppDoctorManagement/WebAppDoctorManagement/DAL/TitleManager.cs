﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAppDoctorManagement.Models;

namespace WebAppDoctorManagement.DAL
{
  
   public class TitleManager : IDisposable
    {
        private readonly ApplicationDbContext _context = new ApplicationDbContext();

        public List<Title> GetAllTitle()
        {
            return _context.Titles.ToList();
        }

        #region IDisposable Support
        private bool disposedValue = false;

        private void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing && _context != null)
                {
                    _context.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion IDisposable Support
    }
}
