﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.ApplicationInsights;

namespace WebAppDoctorManagement.Filters
{
    public class ExceptionLoggingFilter : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            // to explain this first if:
            // http://weblogs.asp.net/leftslipper/httpcontext-iscustomerrorenabled-one-of-asp-net-s-hidden-gems
            if (filterContext.ExceptionHandled)
            {
                // if the exception is already handled, or the custom errors mode is not set, return
                // we should step in here in the dev environment
                return;
            }

            if (new HttpException(null, filterContext.Exception).GetHttpCode() != 500)
            {
                return;
            }

            if (!ExceptionType.IsInstanceOfType(filterContext.Exception))
            {
                return;
            }

            var props = new Dictionary<string, string>();
            if (filterContext.Exception is System.Data.Entity.Validation.DbEntityValidationException)
            {
                var ex = filterContext.Exception as System.Data.Entity.Validation.DbEntityValidationException;
                foreach (var item in ex.EntityValidationErrors)
                {
                    // doing GetType since name(item.Entry.Entity) return "Entity"
                    props.Add("Entity", item.Entry.Entity.GetType().Name);
                    foreach (var err in item.ValidationErrors)
                    {
                        props.Add(err.PropertyName, err.ErrorMessage);
                    }
                }
            }

            // if the request is AJAX return JSON else view.
            if (filterContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                filterContext.Result = new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new
                    {
                        error = true,
                        message = filterContext.Exception.Message
                    }
                };
            }
            else
            {
                var controllerName = (string)filterContext.RouteData.Values["controller"];
                var actionName = (string)filterContext.RouteData.Values["action"];
                var model = new HandleErrorInfo(filterContext.Exception, controllerName, actionName);

                filterContext.Result = new ViewResult
                {
                    ViewName = View,
                    MasterName = Master,
                    ViewData = new ViewDataDictionary<HandleErrorInfo>(model),
                    TempData = filterContext.Controller.TempData
                };
            }

            var telemetryClient = new TelemetryClient();
            telemetryClient.TrackException(filterContext.Exception, props);

            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.StatusCode = 500;

            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
        }
    }
}