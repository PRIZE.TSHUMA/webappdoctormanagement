namespace WebAppDoctorManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DoctorProfile : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DoctorProfiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        Surname = c.String(nullable: false, maxLength: 100),
                        HPCSANUMBER = c.String(nullable: false, maxLength: 20),
                        IDNumber = c.String(nullable: false, maxLength: 20),
                        Discipline = c.Boolean(nullable: false),
                        TitleId = c.Int(nullable: false),
                        ProvinceId = c.Int(nullable: false),
                        RegionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Provinces", t => t.ProvinceId, cascadeDelete: true)
                .ForeignKey("dbo.Regions", t => t.RegionId, cascadeDelete: true)
                .ForeignKey("dbo.Titles", t => t.TitleId, cascadeDelete: true)
                .Index(t => t.TitleId)
                .Index(t => t.ProvinceId)
                .Index(t => t.RegionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DoctorProfiles", "TitleId", "dbo.Titles");
            DropForeignKey("dbo.DoctorProfiles", "RegionId", "dbo.Regions");
            DropForeignKey("dbo.DoctorProfiles", "ProvinceId", "dbo.Provinces");
            DropIndex("dbo.DoctorProfiles", new[] { "RegionId" });
            DropIndex("dbo.DoctorProfiles", new[] { "ProvinceId" });
            DropIndex("dbo.DoctorProfiles", new[] { "TitleId" });
            DropTable("dbo.DoctorProfiles");
        }
    }
}
