﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAppDoctorManagement.Models
{
    public class DoctorProfileViewModel
    {
        [Required]
        public int Id { get; set; }

        [MaxLength(100)]
        [Required]
        public string FirstName { get; set; }

        [MaxLength(100)]
        [Required]
        public string Surname { get; set; }

        [MaxLength(20)]
        [Required]
        public string HPCSANUMBER { get; set; }


        [MaxLength(20)]
        [Required]
        public string IDNumber { get; set; }

        public bool Discipline { get; set; }

        [Required]
        public int TitleId { get; set; }
        public Title Title { get; set; }

        [Required]
        public int ProvinceId { get; set; }
        public Province Province { get; set; }

        [Required]
        public int RegionId { get; set; }
        public Region Region { get; set; }
    }
}
