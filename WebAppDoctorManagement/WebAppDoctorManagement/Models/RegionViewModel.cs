﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAppDoctorManagement.Models
{
    public class RegionViewModel
    {
        [Required]
        public int Id { get; set; }
        [MaxLength(100)]
        [Required]
        public string Description { get; set; }
    }
}
